import datetime

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from .models import Question

class QuestionModelTests(TestCase):
    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is older than 1 day.
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """
        was_published_recently() returns True for questions whose pub_date
        is within the last day.
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

def create_question(question_text, days):
    """
    cria uma questão com um texto e um número de dias, o qual pode ser positivo ou negativo,
    contatos a partir do dia corrente
    """
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)

class QuestionIndexViewTests(TestCase):
    def test_no_questions(self):
        """
        se não existirem questões é exibibida uma mensagem específica.
        """
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Nenhuma questão disponível.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_past_question(self):
        """
        questões com data de publicação no passado são exibidas na index
        """
        create_question(question_text="Questão no passado.", days=-30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Questão no passado.>']
        )

    def test_future_question(self):
        """
        questões com data de pub no futuro não são exibidas na index
        """
        create_question(question_text="Questão no futuro.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertContains(response, "Nenhuma questão disponível.")
        self.assertQuerysetEqual(response.context['latest_question_list'], [])

    def test_future_question_and_past_question(self):
        """
        apenas questões com datas de pub no passado são exibidas
        """
        create_question(question_text="Questão no passado.", days=-30)
        create_question(question_text="Questão no futuro.", days=30)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Questão no passado.>']
        )

    def test_two_past_questions(self):
        """
        são exibidas mais de uma questão com datas de pub no passado
        """
        create_question(question_text="Questão no passado 1.", days=-30)
        create_question(question_text="Questão no passado 2.", days=-5)
        response = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(
            response.context['latest_question_list'],
            ['<Question: Questão no passado 2.>', '<Question: Questão no passado 1.>']
        )

class QuestionDetailView(TestCase):
    def test_future_question(self):
        """
        deverá retornar um erro 404 para questões com data no futuro
        """
        future_question = create_question(question_text='Questão no futuro.', days=5)
        url = reverse('polls:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        deverá exibir corretamente as questões com data no passado
        """
        past_question = create_question(question_text='Questão no passado.', days=-5)
        url = reverse('polls:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)
