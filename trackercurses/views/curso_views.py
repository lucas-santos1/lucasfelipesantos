from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from ..forms import CursoForm
from ..entidades.curso import Curso
from ..services import curso_service

# Create your views here.


@login_required()
def listar_cursos(request):
    cursos = curso_service.listar_cursos(request.user)
    return render(request, 'cursos/listar_cursos.html', {"cursos": cursos})


@login_required()
def cadastrar_curso(request):
    if request.method == "POST":
        form_curso = CursoForm(request.POST)
        if form_curso.is_valid():
            titulo = form_curso.cleaned_data["titulo"]
            descricao = form_curso.cleaned_data["descricao"]
            data_expiracao = form_curso.cleaned_data["data_expiracao"]
            prioridade = form_curso.cleaned_data["prioridade"]
            curso_nova = Curso(titulo=titulo, descricao=descricao, data_expiracao=data_expiracao,
                                 prioridade=prioridade, usuario=request.user)
            curso_service.cadastrar_curso(curso_nova)
            return redirect('listar_cursos')
    else:
        form_curso = CursoForm()
    return render(request, 'cursos/form_curso.html', {"form_curso": form_curso})


@login_required()
def editar_curso(request, id):
    curso_bd = curso_service.listar_curso_id(id)
    if curso_bd.usuario != request.user:
        return HttpResponse("Não permitido")
    form_curso = CursoForm(request.POST or None, instance=curso_bd)
    if form_curso.is_valid():
        titulo = form_curso.cleaned_data["titulo"]
        descricao = form_curso.cleaned_data["descricao"]
        data_expiracao = form_curso.cleaned_data["data_expiracao"]
        prioridade = form_curso.cleaned_data["prioridade"]
        curso_nova = Curso(titulo=titulo, descricao=descricao, data_expiracao=data_expiracao,
                             prioridade=prioridade, usuario=request.user)
        curso_service.editar_curso(curso_bd, curso_nova)
        return redirect('listar_cursos')
    return render(request, 'cursos/form_curso.html', {"form_curso": form_curso})


@login_required()
def remover_curso(request, id):
    curso_bd = curso_service.listar_curso_id(id)
    if curso_bd.usuario != request.user:
        return HttpResponse("Não permitido")
    if request.method == "POST":
        curso_service.remover_curso(curso_bd)
        return redirect('listar_cursos')
    return render(request, 'cursos/confirma_exclusao.html', {'curso': curso_bd})
