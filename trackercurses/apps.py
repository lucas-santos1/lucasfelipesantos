from django.apps import AppConfig


class TrackercursesConfig(AppConfig):
    name = 'trackercurses'
