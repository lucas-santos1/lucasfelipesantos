from ..models import Curso


def cadastrar_curso(curso):
    Curso.objects.create(titulo=curso.titulo, descricao=curso.descricao,
                          data_expiracao=curso.data_expiracao, prioridade=curso.prioridade,
                          usuario=curso.usuario)

def listar_cursos(usuario):
    return Curso.objects.filter(usuario=usuario).all()

def listar_curso_id(id):
    return Curso.objects.get(id=id)

def editar_curso(curso_bd, curso_nova):
    curso_bd.titulo = curso_nova.titulo
    curso_bd.descricao = curso_nova.descricao
    curso_bd.data_expiracao = curso_nova.data_expiracao
    curso_bd.prioridade = curso_nova.prioridade
    curso_bd.save(force_update=True)

def remover_curso(curso_bd):
    curso_bd.delete()