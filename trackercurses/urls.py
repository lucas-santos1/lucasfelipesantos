from django.urls import path
from .views.curso_views import *
from .views.usuario_views import *

app_name = 'trackercurses'
urlpatterns = [
    path('', listar_cursos, name='listar_cursos'),
    path('listar_cursos/', listar_cursos, name='listar_cursos'),
    path('cadastrar_curso/', cadastrar_curso, name="cadastrar_curso"),
    path('editar_curso/<int:id>', editar_curso, name="editar_curso"),
    path('remover_curso/<int:id>', remover_curso, name="remover_curso"),
    path('cadastrar_usuario/', cadastrar_usuario, name="cadastrar_usuario"),
    path('logar_usuario/', logar_usuario, name="logar_usuario"),
    path('deslogar_usuario/', deslogar_usuario, name="deslogar_usuario"),
]
